
## 0.6.4 [10-15-2024]

* Changes made at 2024.10.14_21:22PM

See merge request itentialopensource/adapters/adapter-onap_aai!17

---

## 0.6.3 [09-13-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-onap_aai!15

---

## 0.6.2 [08-14-2024]

* Changes made at 2024.08.14_19:39PM

See merge request itentialopensource/adapters/adapter-onap_aai!14

---

## 0.6.1 [08-07-2024]

* Changes made at 2024.08.06_21:43PM

See merge request itentialopensource/adapters/adapter-onap_aai!13

---

## 0.6.0 [05-10-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!12

---

## 0.5.4 [03-26-2024]

* Changes made at 2024.03.26_14:47PM

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!11

---

## 0.5.3 [03-13-2024]

* Changes made at 2024.03.13_14:04PM

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!10

---

## 0.5.2 [03-11-2024]

* Changes made at 2024.03.11_14:13PM

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!9

---

## 0.5.1 [02-26-2024]

* Changes made at 2024.02.26_13:41PM

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!8

---

## 0.5.0 [12-30-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!7

---

## 0.4.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!6

---

## 0.3.3 [03-11-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!5

---

## 0.3.2 [07-08-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!4

---

## 0.3.1 [01-13-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!3

---

## 0.3.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!2

---

## 0.2.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-onap_aai!1

---

## 0.1.1 [08-16-2019]

- Initial Commit

See commit 64feb9e

---

# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Onap_aai System. The API that was used to build the adapter for Onap_aai is usually available in the report directory of this adapter. The adapter utilizes the Onap_aai API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The ONAP Active and Available Inventory adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ONAP Active and Available Inventory DDI to offer inventory and topology for configuration and orchestration. 

With this adapter you have the ability to perform operations with ONAP Active and Available Inventory such as:

- Network
- Cloud Infrastructure

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
